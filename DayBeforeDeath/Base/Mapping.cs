﻿using System;
using AutoMapper;
using DayBeforeDeath.ViewModels;

public class Mapping
{
    public void Create()
    {
        Mapper.CreateMap<DataModel, DataViewModel>();
        Mapper.CreateMap<DataViewModel, DataModel>();

        Mapper.CreateMap<Setting, SettingViewModel>();
        Mapper.CreateMap<SettingViewModel, Setting>();
    }
}

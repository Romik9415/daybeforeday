﻿using System;
using AutoMapper;
using DayBeforeDeath.ViewModels;

static public class ModelSettings
{
    static private DataModel _model { get; set; }
    private static DataViewModel _viewModel { get; set; }

    static public DataModel getDataModel()
    {
        return _model;
    }

    static public DataModel loadModel()
    {
        _model = DataModel.Load();
        return _model;
    }

    static public DataViewModel loadViewModel(DataModel dataModel)
    {
        _viewModel = Mapper.Map<DataModel, DataViewModel>(dataModel);
        return _viewModel;
    }

    static public DataModel saveModel(DataModel dataModel)
    {
        new Mapping().Create();//DOTO change automaper
        _model = Mapper.Map<DataViewModel, DataModel>(_viewModel);
        //_model.Save();
        return dataModel;
    }
    //_model = ModelSettings.loadModel();
    //_viewModel = Mapper.Map<DataModel, DataViewModel>(_model);


}

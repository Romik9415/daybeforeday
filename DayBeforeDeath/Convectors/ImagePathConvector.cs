﻿using System;
using System.Drawing;
using System.Globalization;
using System.Net;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DayBeforeDeath.Convectors
{

    class ImagePathConvector : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var value_type = (ValueType) value;
            var uri = string.Format("C:\\Users\\Roman\\source\\repos\\DayBeforeDeath\\DayBeforeDeath\\resurses\\buttonImages\\{0}_icon.png", value_type.ToString().ToLower());
        //   var uri = string.Format(@"resurses/buttonImages/{0}_icon.png", value_type.ToString().ToLower());
            return uri;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
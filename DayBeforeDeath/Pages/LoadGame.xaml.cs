﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DayBeforeDeath.Pages
{
    /// <summary>
    /// Interaction logic for LoadGame.xaml
    /// </summary>
    public partial class LoadGame : Page
    {
        System.Media.SoundPlayer player = new SoundPlayer(Properties.Resources.bimp);
        private bool soundsSettingOn;

        public LoadGame()
        {
            InitializeComponent();
            var a = ModelSettings.getDataModel();
            soundsSettingOn = a.Settings.First().Value == Setting.ValueStatus.ON;
        }

        private void Back_Button(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
            playSound();
        }

        private void playSound()
        {
            if (soundsSettingOn)
            {
                player.Play();
            }
        }

    }
}

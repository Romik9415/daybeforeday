﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.Properties;

namespace DayBeforeDeath
{

    public partial class MainWindow : Window
    {
        System.Media.SoundPlayer player = new SoundPlayer(Properties.Resources.startSound);

        public MainWindow()
        {
            InitializeComponent();
            Main.Content = new MainPage();
            var a = ModelSettings.getDataModel();
            if (a.Settings.ElementAt(1).Value == Setting.ValueStatus.ON)
            {
                player.Play();
            }
        }

        private void Main_OnNavigated(object sender, NavigationEventArgs e)
        {
        
           
        }
        public MainWindow getMainWindow()
        {
            return this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Main_Navigated(object sender, NavigationEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           // var setting = (Settings)DataGridSettings2.CurrentItem;

        }

        private void SoundsControls_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void backGroundCnahge (object sender, RoutedEventArgs e)
        {
            Button1.Background = null;
            Button2.Background = null;
            Button3.Background = null;
            Button btn = sender as Button;
            btn.Background = Brushes.White;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.serialization.PlayerSerialization;

namespace DayBeforeDeath
{
    /// <summary>
    /// Interaction logic for NewGame.xaml
    /// </summary>
        public partial class NewGame : Page
        {
            System.Media.SoundPlayer player = new SoundPlayer(Properties.Resources.bimp);
            private bool soundsSettingOn;

            public NewGame()
            {
                InitializeComponent();
            var a = ModelSettings.getDataModel();
            soundsSettingOn = a.Settings.First().Value == Setting.ValueStatus.ON;

        }

            private void Button_Click(object sender, RoutedEventArgs e)
            {
                this.NavigationService.Navigate(new MainPage());
                playSound();
            }

            private void Button_Click_1(object sender, RoutedEventArgs e)
            {
                playSound();
                TextBox newUserName = (TextBox)this.FindName("NewUserName");
                int diff = 0;
                if (RadioButtonEasy.IsChecked == true)
                {
                    diff = 0;
                }
                if (RadioButtonNormal.IsChecked == true)
                {
                    diff = 1;
                }
                if (RadioButtonHard.IsChecked == true)
                {
                    diff = 2;

                }
                if (diff != null)
                {
                    String sName = newUserName.Text;
                    if (sName != "")
                    {
                        //Change difficalty
                       // Player newPlayer = new Player(sName,1,500,Player.Edifficulty.Normal);
                       Player newPlayer = new Player(){name = sName,lvl = 0,money = 500,difficulty = Player.Edifficulty.Normal};
                       // PlayerDataModel.Load().AddPlayer(newPlayer);


                        this.NavigationService.Navigate(new StorryPage());
                        changeBGtoGray();
                    }
                    else
                    {
                        MessageBox.Show("You mast enter your name");

                    }

                }
                else
                {
                    MessageBox.Show("You must select difficulty level");
                
            }

            }
            private void playSound()
            {
                if (soundsSettingOn)
                {
                    player.Play();
                }
            }

            private void changeBGtoGray()
            {
                MainWindow mw = (MainWindow) Application.Current.MainWindow;
            var brush = new SolidColorBrush(Color.FromArgb(255, (byte)39, (byte)39, (byte)39));
            mw.Background = brush;
                mw.bgImage.Opacity = 0;
            }
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DayBeforeDeath.Pages.StoryPage
{
    /// <summary>
    /// Interaction logic for StoryPage3.xaml
    /// </summary>
    public partial class StoryPage3 : Page
    {
        public int buttonNextClicked = 0;
        public StoryPage3()
        {
            InitializeComponent();
        }
        private void Button_next(object sender, RoutedEventArgs e)
        {
            if (buttonNextClicked == 0)
            {
                LabelStory1.Content = "At midnight there were lights\neverywhere because people were painting\nthe town red. And the next day pretty\nmuch the same.";

            }
            if (buttonNextClicked == 1)
            {
                LabelStory1.Content = "At midnight there were lights\neverywhere because people were painting\nthe town red. And the next day pretty\nmuch the same.\nI love NY. Loved...";
            }
            if (buttonNextClicked == 2)
            {
                this.NavigationService.Navigate(new StoryPage4());
            }
            buttonNextClicked++;

        }
    }
}

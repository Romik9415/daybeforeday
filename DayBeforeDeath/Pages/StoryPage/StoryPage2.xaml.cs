﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DayBeforeDeath.Pages.StoryPage
{
    /// <summary>
    /// Interaction logic for StoryPage2.xaml
    /// </summary>
    public partial class StoryPage2 : Page
    {
        public int buttonNextClicked = 0;
        public StoryPage2()
        {
            InitializeComponent();
        }
        private void Button_next(object sender, RoutedEventArgs e)
        {
            if (buttonNextClicked == 0)
            {
                LabelStory1.Content = "I lived in a city that we called New York.\nIt seems that this city never sleepped.";

            }
            if (buttonNextClicked == 1)
            {
                LabelStory1.Content = "I lived in a city that we called New York.\nIt seems that this city never sleepped.\nStarting from 6am streets was crowded by\npeople, who were in a hurry\nnot to late for work.";
            }
            if (buttonNextClicked == 2)
            {
                this.NavigationService.Navigate(new StoryPage3());
            }
            buttonNextClicked++;

        }
    }
}

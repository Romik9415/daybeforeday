﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.Pages.StoryPage;

namespace DayBeforeDeath.Pages
{
    /// <summary>
    /// Interaction logic for StoryPage5.xaml
    /// </summary>
    public partial class StoryPage5 : Page
    {
        public int buttonNextClicked = 0;
        public StoryPage5()
        {
            InitializeComponent();
        }

        private void Button_next(object sender, RoutedEventArgs e)
        {
            if (buttonNextClicked == 0)
            {
                LabelStory1.Content =
                    "Today, it has nothin similar to That.\nToday, it looks like the desert.\nNo, without camels and sand.\nIt's modern desert.\nWith reminder of\nbusiness centers, cafes and\nparks. With reminder of place that\ni used to call 'Home'...";

            }
            if (buttonNextClicked == 1)
            {
                LabelStory1.Content =
                    "Today, it has nothin similar to That.\nToday, it looks like the desert.\nNo, without camels and sand.\nIt's modern desert.\nWith reminder of\nbusiness centers, cafes and\nparks. With reminder of place that\ni used to call 'Home'...\nWho, if not me? \nWhen, if not now? I\nstudied machines in college\nand i know something about\nit, so, i was able to create\ncloning machine and create an army\nthat consists of ME.\nCool, isn't it? But how can i\nfight against these monsters\nwithout weapon?";
            }
            if (buttonNextClicked == 2)
            {
                this.NavigationService.Navigate(new StoryPage6());
            }
            buttonNextClicked++;

        }

    }
}

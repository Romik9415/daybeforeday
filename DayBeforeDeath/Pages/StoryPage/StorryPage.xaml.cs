﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.Pages.StoryPage;
using Ninject.Syntax;

namespace DayBeforeDeath
{
    /// <summary>
    /// Interaction logic for StorryPage.xaml
    /// </summary>
    public partial class StorryPage : Page
    {
        public int buttonNextClicked = 0;
        public StorryPage()
        {
            InitializeComponent();
          
        }

        private void Button_next(object sender, RoutedEventArgs e)
        {
            if (buttonNextClicked==0)
            {
                LabelStory1.Content = "Everyone ones have a place what they \ncalled 'home'. I also had such place.";
                
            }
            if (buttonNextClicked == 1)
            {
                LabelStory1.Content = "Everyone ones have a place what they \ncalled 'home'. I also had such place.\nHad...";
            }
            if (buttonNextClicked == 2)
            {
                this.NavigationService.Navigate(new StoryPage2());
            }
            buttonNextClicked++;

        }
    }
}

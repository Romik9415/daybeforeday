﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DayBeforeDeath.Pages.StoryPage
{
    /// <summary>
    /// Interaction logic for StoryPage6.xaml
    /// </summary>
    public partial class StoryPage6 : Page
    {
       

        public StoryPage6()
        {
            InitializeComponent();

        }

        private void Button_next(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Game());
        }
    }
}

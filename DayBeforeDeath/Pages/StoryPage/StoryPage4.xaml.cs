﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.Pages.StoryPage;

namespace DayBeforeDeath.Pages
{
    /// <summary>
    /// Interaction logic for StoryPage4.xaml
    /// </summary>
    public partial class StoryPage4 : Page
    {
        public int buttonNextClicked = 0;
        public StoryPage4()
        {
            InitializeComponent();
        }
        private void Button_next(object sender, RoutedEventArgs e)
        {
            if (buttonNextClicked == 0)
            {
                LabelStory1.Content = "New NY looks like this.\nWithout any color.\nAny coffeeshops. Any human.\nIt seems i am the last one here.\nLast man who can survive. Last man\nwho will fight against our invadors.";

            }
            if (buttonNextClicked == 1)
            {
                LabelStory1.Content = "New NY looks like this.\nWithout any color.\nAny coffeeshops. Any human.\nIt seems i am the last one here.\nLast man who can survive. Last man\nwho will fight against our invadors.\nZombies. Brrr.\nSounds horrible. ";
            }
            if (buttonNextClicked == 2)
            {
                this.NavigationService.Navigate(new StoryPage5());
            }
            buttonNextClicked++;

        }
    }
}

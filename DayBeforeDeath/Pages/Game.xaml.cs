﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.Pages.StoryPage;

namespace DayBeforeDeath.Pages
{
    
    public partial class Game : Page
    {
        private bool buttonPresed = false;
        private int money = 500;
        public Game()
        {
            InitializeComponent();
        }

        private void buy_button(object sender, RoutedEventArgs e)
        {
         
        
           if (buttonPresed){
               animation(-70);
               buttonPresed = false;
               buyButtonImage.Source = new BitmapImage(new Uri("C:\\Users\\Roman\\source\\repos\\DayBeforeDeath\\DayBeforeDeath\\resurses\\gameImages\\ic_person_add_white_24dp_2x.png"));
            }
           else
           {
               animation(0);
               buttonPresed = true;
               buyButtonImage.Source = new BitmapImage(new Uri("C:\\Users\\Roman\\source\\repos\\DayBeforeDeath\\DayBeforeDeath\\resurses\\gameImages\\ic_keyboard_backspace_white_24dp_2x.png"));
           }
            ;
        }

        void animation(int to)
        {
            while (sliX.Value!=to)
            {
                if (sliX.Value < to)
                {
                    sliX.Value = sliX.Value + 1;
                    
                }
                else
                {
                    sliX.Value = sliX.Value - 1;
                    
                }
            }
        }



        private void save_button(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Saved!");
        }

        private void play_button(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You win");
            delUnitTo(LabelP1);
            delUnitTo(LabelP1);
            delUnitTo(LabelP1);
            delUnitTo(LabelP2);
            delUnitTo(LabelP3);
            delUnitTo(LabelP3);
            moneyadd(400);
            money+=400;
            LevelNumber.Content = Int32.Parse(LevelNumber.Content.ToString())+1;

        }

        private void exit_button(object sender, RoutedEventArgs e)
        {
            string sMessageBoxText = "Do you want exit?";
            string sCaption = "Exit";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNo;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

            switch (rsltMessageBox)
            {
                case MessageBoxResult.Yes:

                    MainWindow mw = (MainWindow)Application.Current.MainWindow;
                    var brush = new SolidColorBrush(Color.FromArgb(255, (byte)252, (byte)92, (byte)107));
                    mw.Background = brush;
                    mw.bgImage.Opacity = 1;

                    this.NavigationService.Navigate(new MainPage());
                    break;

                case MessageBoxResult.No:
                    /* ... */
                    break;

            }

        }

        private void buy_1(object sender, RoutedEventArgs e)
        {
            moneyDecrease(10, LabelP1);
            
            
        }

        private void buy_2(object sender, RoutedEventArgs e)
        {
            moneyDecrease(30, LabelP2);
            
        }

        private void buy_3(object sender, RoutedEventArgs e)
        {
            moneyDecrease(50, LabelP3);
            
        }

        private void buy_4(object sender, RoutedEventArgs e)
        {
            moneyDecrease(60, LabelP4);
            
        }

        private void buy_5(object sender, RoutedEventArgs e)
        {
            moneyDecrease(80, LabelP5);
            
        }

        private void buy_6(object sender, RoutedEventArgs e)
        {
            moneyDecrease(100, LabelP6);
            
        }

        void moneyDecrease(int momeyDe,Label label)
        {
            if (money - momeyDe >= 0)
            {
                int total = money - momeyDe;
                money -= momeyDe;
                Money.Content = total.ToString()+ "$";

                addUnitTo(label);
            }
            else
            {
                MessageBox.Show("Not money, no honey");
            }
        }

        void moneyadd(int amAdd)
        {
            int total = money + amAdd;
                Money.Content = total.ToString() + "$";

          
          
        }

        void addUnitTo(Label label)
        {
            int count = Int32.Parse(label.Content.ToString());
            count++;
            label.Content = count;
        }

        void delUnitTo(Label label)
        {
           int count = Int32.Parse(label.Content.ToString());
            if (count > 0)
            {
                count--;
                label.Content = count;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.Pages;

namespace DayBeforeDeath
{

    public partial class MainPage : Page
    {
        System.Media.SoundPlayer player = new SoundPlayer(Properties.Resources.bimp);
        private bool soundsSettingOn;

        public MainPage()
        {
            InitializeComponent();
            var wnd = Window.GetWindow(this);
            var a = ModelSettings.getDataModel();
            soundsSettingOn = a.Settings.First().Value == Setting.ValueStatus.ON;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var wnd = Window.GetWindow(this);
            wnd.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            playSound();
            this.NavigationService.Navigate(new SettingsPage());
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            playSound();
            this.NavigationService.Navigate(new NewGame());
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            playSound();
            var p = new CreditsPage();
            p.DataContext = this.DataContext;
            this.NavigationService.Navigate(p);
        }

        private void playSound()
        {
            if (soundsSettingOn)
            {
                player.Play();
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            playSound();
            this.NavigationService.Navigate(new LoadGame());
        }
    }
}

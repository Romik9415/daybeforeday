﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DayBeforeDeath.ViewModels;

namespace DayBeforeDeath
{
    public partial class SettingsPage : Page
    {

      
        System.Media.SoundPlayer player = new SoundPlayer(Properties.Resources.bimp);
        public Grid grid;
        public DataGrid dataGrid;
        private bool soundsSettingOn;


        public SettingsPage()
        {
            InitializeComponent();
            var mainWindow = Application.Current.MainWindow;
            //car SettingView = DayBeforeDeath.Views.SoundsControls;
            grid = (Grid) mainWindow.FindName("DataGridSettings2");
            grid.Visibility = Visibility.Visible;
            var a = ModelSettings.getDataModel();
            soundsSettingOn = a.Settings.First().Value == Setting.ValueStatus.ON;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ModelSettings.saveModel(ModelSettings.getDataModel().Update());
            this.NavigationService.Navigate(new MainPage());
            grid.Visibility = Visibility.Hidden;
            playSound();

        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
        }
        private void playSound()
        {
            if (soundsSettingOn)
            {
                player.Play();
            }
        }
    }
}

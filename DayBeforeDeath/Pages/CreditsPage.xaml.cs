﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DayBeforeDeath
{
    /// <summary>
    /// Interaction logic for CreditsPage.xaml
    /// </summary>
    public partial class CreditsPage : Page
    {
        System.Media.SoundPlayer player = new SoundPlayer(Properties.Resources.bimp);
        private bool soundsSettingOn;
        public CreditsPage()
        {
            InitializeComponent();
            var a = ModelSettings.getDataModel();
            soundsSettingOn = a.Settings.First().Value == Setting.ValueStatus.ON;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainPage());
            playSound();
        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //Easter egg
            System.Media.SoundPlayer playerEgg = new SoundPlayer(Properties.Resources.bgSound);
            playerEgg.Play();
        }
        private void playSound()
        {
            if (soundsSettingOn)
            {
                player.Play();
            }
        }
    }
}

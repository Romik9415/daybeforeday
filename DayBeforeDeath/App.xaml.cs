﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using DayBeforeDeath.ViewModels;
using Ninject;

namespace DayBeforeDeath
{
    public partial class App : Application
    {
        private DataModel _model { get; set; }
        private DataViewModel _viewModel { get; set; }

        public App()
        {
            new Mapping().Create();
            _model = ModelSettings.loadModel();
            _viewModel = ModelSettings.loadViewModel(_model);
            initAutomapper();

            var window = new MainWindow()
            {
                DataContext = _viewModel
            };
            window.Show();

        }
        private void initAutomapper()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DataModel, DataViewModel>();

            });
        }

        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                new Mapping().Create();
                _model = Mapper.Map<DataViewModel, DataModel>(_viewModel);
                _model.Save();
            }
            catch (Exception)
            {
                base.OnExit(e);
                throw;
            }
        }

        public DataViewModel getModel()
        {
            return _viewModel;
        }
    }


}

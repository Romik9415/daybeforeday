﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DayBeforeDeath.serialization.PlayerSerialization
{
    class PlayerDataSerializer
    {
        public static void SerializeData(String fileName, PlayerDataModel data)
        {
            var formatter = new DataContractSerializer(typeof(PlayerDataModel));
            var s = new FileStream(fileName, FileMode.Create);
            formatter.WriteObject(s, data);
            s.Close();
        }

        public static PlayerDataModel DesirilizeItem(String filename)
        {
            var s = new FileStream(filename, FileMode.Open);
            var formatter = new DataContractSerializer(typeof(PlayerDataModel));
            return (PlayerDataModel)formatter.ReadObject(s);
        }
    }
}

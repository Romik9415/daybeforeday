﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DayBeforeDeath.serialization.PlayerSerialization
{
    [DataContract]
    class Player
    {
        [DataMember]
        public String name { get; set; }
        [DataMember]
        public int lvl { get; set; }
        [DataMember]
        public int money { get; set; }
        [DataMember]
        public Edifficulty difficulty { get; set; }


        [DataContract]
        public enum Edifficulty
        {
            [EnumMember]
            Easy,
            [EnumMember]
            Normal,
            [EnumMember]
            Hard
        };

        //public Player(String name, int lvl, int money, Edifficulty difficulty)
        //{
        //    this.name = name;
        //    this.lvl = lvl;
        //    this.money = money;
        //    this.difficulty = difficulty;
        //}


    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DayBeforeDeath.serialization.PlayerSerialization
{
    [DataContract]
    class PlayerDataModel
    {
        [DataMember]
        public IEnumerable<Player> PlayerList { get; set; }

        public static string DataPath = "C:/Users/Roman/Desktop/Resilizer/playerList.dat";

        public PlayerDataModel()
        {
           
        }

        public void AddPlayer(Player player)
        {
            
            if (PlayerList == null)
            {
                PlayerList = new List<Player>(){player};
            }
            else
            {
                List<Player> playerListTemp;
                playerListTemp = PlayerList.ToList();
                playerListTemp.Add(player);
                PlayerList = playerListTemp;
            }
            Save();
            
        }

        public static PlayerDataModel Load()
        {
            if (File.Exists(DataPath))
            {
                return PlayerDataSerializer.DesirilizeItem(DataPath);
                
            }
            return new PlayerDataModel();

        }

        public PlayerDataModel getDataModel()
        {
            return this;
        }


        public void Save()
        {
            PlayerDataSerializer.SerializeData(DataPath, this);
        }

        public PlayerDataModel Update()
        {
            PlayerDataSerializer.SerializeData(DataPath, this);
            return this;
        }

    }
}

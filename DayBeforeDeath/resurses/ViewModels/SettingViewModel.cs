﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5_wpf_sterilization.Serialization;

namespace OrganaizerUi.resurses.ViewModels
{
    public class SettingViewModel:ViewModelBase
    {
     private string _property;
     public string Property {
         get { return _property; }
         set
         {
             _property = value;
             OnPropertyChanged("Property");
         }
     }

        private Setting.ValueStatus _value;

        public Setting.ValueStatus Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

}
}

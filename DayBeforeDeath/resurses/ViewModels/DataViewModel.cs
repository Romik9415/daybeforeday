﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganaizerUi.resurses.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        private ObservableCollection<SettingViewModel> _settings;

        public ObservableCollection<SettingViewModel> Settings
        {
            get { return _settings; }
            set
            {
                _settings = value;
                OnPropertyChanged("Settings");
            }
        }
    }
}

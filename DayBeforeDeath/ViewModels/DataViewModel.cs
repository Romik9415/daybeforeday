﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DayBeforeDeath.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        public DataViewModel()
        {
            SetControlVisibility = new Command(ControlVisibility);
            ChangeValueCommand = new Command(ChangeValue);
        }

        private String _visibleControl = "Settings";//Change to soundSettibgs

        public string VisibleControl
        {
            get { return _visibleControl; }
            set
            {
                _visibleControl = value;
                OnPropertyChanged("VisibleControl");
            }
        }

        public ICommand SetControlVisibility { get; set; }

        private SettingViewModel _selectedSetting;

        public SettingViewModel SelectedSetting
        {
            get { return _selectedSetting; }
            set
            {
                _selectedSetting = value;
                OnPropertyChanged("SelectedSetting");
            }
        }


        public void ControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }


        public ICommand ChangeValueCommand { get; set; }

        public void ChangeValue(object arg)
        {
           // var setting = (SettingViewModel)arg;

            if (_selectedSetting.Value == Setting.ValueStatus.ON)
            {
                _selectedSetting.Value = Setting.ValueStatus.Off;
            }
            else
            {
                _selectedSetting.Value = Setting.ValueStatus.ON;
            }

        }

        private ObservableCollection<SettingViewModel> _settings;

        public ObservableCollection<SettingViewModel> Settings
        {
            get { return _settings; }
            set
            {
                _settings = value;
                OnPropertyChanged("Settings");
            }
        }
    }
}

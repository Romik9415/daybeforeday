﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace DayBeforeDeath.ViewModels
{
    public class MyCustomControl : Control
    {
        static MyCustomControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MyCustomControl), new FrameworkPropertyMetadata(typeof(MyCustomControl)));
        }

        //Depentency
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color",
            typeof(Color),
            typeof(MyCustomControl),
            new PropertyMetadata(Colors.Green));

        public Color Color
        {
            get
            {
                return (Color)this.GetValue(ColorProperty);
            }
            set
            {
                this.SetValue(ColorProperty, value);
            }
        }
        //Custom command
        public static readonly ICommand CustomCommand = new RoutedUICommand("CustomCommand", "CustomCommand",
            typeof(MyCustomControl),
            new InputGestureCollection(
                new InputGesture[] {
                    new KeyGesture(Key.Enter),
                    new MouseGesture(MouseAction.LeftClick) }
            )
        );

    }
}

﻿using System;

public class DataSerializer
{
    public static void SerializeData(String fileName, DataModel data)
    {
        var formatter = new DataContractSerializer(typeof(DataModel));
        var s = new FileStream(fileName, FileMode.Create);
        formatter.WriteObject(s, data);
        s.Close();
    }

    public static DataModel DesirilizeItem(String filename)
    {
        var s = new FileStream(filename, FileMode.Open);
        var formatter = new DataContractSerializer(typeof(DataModel));
        return (DataModel)formatter.ReadObject(s);
    }
}
﻿using System;

[DataContract]
public class DataModel
{
    [DataMember]
    public IEnumerable<Setting> Settings { get; set; }

    public static string DataPath = "C:/Users/Roman/Desktop/Resilizer/settings.dat";

    public DataModel()
    {
        Settings = new List<Setting>() { new Setting() { Property = "Sounds", Value = Setting.ValueStatus.ON }, new Setting() { Property = "Moving Backgroun", Value = Setting.ValueStatus.Off } };
    }

    public static DataModel Load()
    {
        if (File.Exists(DataPath))
        {
            return DataSerializable.DataSerializer.DesirilizeItem(DataPath);
        }
        return new DataModel();

    }

    public DataModel getDataModel()
    {
        return this;
    }


    public void Save()
    {
        DataSerializable.DataSerializer.SerializeData(DataPath, this);
    }

    public DataModel Update()
    {
        DataSerializable.DataSerializer.SerializeData(DataPath, this);
        return this;
    }

}


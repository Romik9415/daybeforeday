﻿using System;

[DataContract]
public class Setting
{
    [DataMember]
    public string Property { get; set; }
    [DataMember]
    public ValueStatus Value { get; set; }

    [DataContract]
    public enum ValueStatus
    {
        [EnumMember]
        ON,
        [EnumMember]
        Off
    }
}

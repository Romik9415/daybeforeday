﻿using System;

class ImagePathConvector : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var value_type = (ValueType)value;
        var uri = new Uri(string.Format(@"resurses/buttonImages/{0}_icon.png", value_type.ToString().ToLower()), UriKind.Relative);
        return new BitmapImage(uri);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}